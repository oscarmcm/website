AUTHOR = 'Oscar Cortez'
SITENAME = 'Oscar\'s Blog'
SITEURL = ''
PATH = 'content'
THEME = 'theme'
TIMEZONE = 'America/Bogota'

DEFAULT_LANG = 'en'

CATEGORY_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
ARCHIVES_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
TAGS_SAVE_AS = ''
ARTICLE_SAVE_AS = 'essays/{date:%Y}/{slug}/index.html'
ARTICLE_URL = '/essays/{date:%Y}/{slug}/'
DEFAULT_LANG = 'en'
USE_FOLDER_AS_CATEGORY = True
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
DIRECT_TEMPLATES = ['index', 'categories', 'essays']

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
