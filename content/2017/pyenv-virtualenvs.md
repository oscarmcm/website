Title: Pyenv y Virtualenv
Date: 2016-06-08
Tags: es

Python 3 ha estado ahí desde hace algún tiempo, pero la principal excusa por no usarlo siempre
ha sido la falta de apoyo de los proyectos de 3ro en parte.
En 2014-2015 esto cambió drásticamente y parece que es un
buen momento para empezar a hacer algo serio con Python 3.

Mi mayor problema es que tengo un montón de código Python 2 y que no esta probado con Python 3.
Antes de Python 3 el mundo era fácil.
He instalado Python 2 usando brew y virtualenv + virtualenvwrapper para aislar
todas las dependencias de mis proyectos ha hecho de este simple y fácil …

Pero cuando empiezas a usar múltiples versiones de Python te das cuenta
que las cosas se complican. Es necesario algo así como virtualenv con soporte
para aislar la instalación de Python en sí, de manera que se pueden utilizar
diferentes versiones en función de cada proyecto y de sus dependencias.
Como se pueden imaginar pyenv es exactamente eso. Se elimina la dificultad de
tener múltiples versiones de Python instalados al mismo tiempo.

Todas las cosas que diré asumo que está utilizando OSX. Linux debería ser ligeramente diferente
y Windows es por desgracia sólo un ciudadano de segunda clase.

### Instalar PyEnv

Como explica el autor de este proyecto en el repositorio de Github, bastara con ejecutar:

```
brew install pyenv
```

### Instalando nuevas versiones de Python

Una vez que tenemos pyenv instalado, podemos instalar nuevas versiones de Python, ejecutando:

```
pyenv install python_version
```

```
pyenv install 3.4.2
```

Ahora puedes verificar las versiones de Python disponibles:

```
$ pyenv versions
* system (set by /Users/foo/.python-version)
* 3.4.2 (set by /Users/foo/.python-version)
```

Una vez que todo esta funcionando, procedemos a instalar virtualenvwrapper y un plugin de pyenv para hacerlos compatibles:

```
$ pip install virtualenvwrapper`}
```

```
$ brew install pyenv-virtualenvwrapper
```

Y ahora agregaremos lo siguiente a nuestro archivo de configuración, si utilizas zsh de interprete de terminal entonces _.zshrc_, si todo esta por defecto entonces _.bash_profile_

```
# pyenv
eval "$(pyenv init -)"
# virtualenvwrapper
export WORKON_HOME=~/.enviroments
source /usr/local/bin/virtualenvwrapper.sh
```

Ahora solo nos falta activar pyenv virtualenvwrapper ejecutando:

```
pyenv virtualenvwrapper`
```

Ahora podemos crear nuestros entornos virtuales sin problema alguno y con la version de python que mas nos guste:

```
mkvirtualenv myenv -p $(which python)
```

```
mkvirtualenv myenv2 -p $(which python3.4)
```

