Title: Django-Places v2
Date: 2017-09-09
Tags: en

Following with the development of my small app, today I released the version 2.

This version add supports for _Django 1.11_ new widgets API and
the new included and required _API KEY_ for Google Maps, also includes the following fixes:

- Update field max length.
- Python2 unicode/decode error.
- Use Jquery nonConflict.
- Stop using regex for parsing the value.
- Set correct kwargs position.

If you want to learn more about it, just check the project in Github
at [oscarmcm/django-places](https://github.com/oscarmcm/django-places)

