Title: Django Test Tags
Date: 2017-07-22
Tags: en

The **Oxford Dictionary** describes the word **tag** as:

> a label attached to someone or something for the purpose of **identification** or to give other information.

In the world of _computers_ and the _internet_ is defined like:

> A word, phrase, or name used to **identify** digital content such as blog and social media posts as belonging to a particular category or concerning a particular person or topic.

Taking those definitions, Django has a hidden _gem_ and
fairly misused tool on his test suite, which help us to **identify** our test cases.

### Tagging Test

This tool will help us to tag our tests, so we can easily run a particular subset of this. For example, you might label fast or slow tests.
The Django **tag** works as _decorator_ in our test case method, so, firstly we need to import our tag decorator.

```
from django.test import tag
```

Now, we can easily use the decorator:

```py
class SampleTestCase(TestCase):
    @tag('fast')
    def test_fast(self):
        ...
    @tag('slow')
    def test_slow(self):
        ...
    @tag('slow', 'core')
    def test_slow_but_core(self):
        ...
```

And also, the tag decorator can be used in the test case per se:

```py
@tag('slow', 'core')
class SampleTestCase(TestCase):
    ...
```

Now we can choose which tests to run. For example, to run only fast tests:

```
./manage.py test --tag=fast`}
```

And also can exclude tests by tag. To run core tests if they are not slow:

```
./manage.py test --tag=core --exclude-tag=slow
```

You can read more about it on [Django Testing Tools](https://docs.djangoproject.com/es/2.1/topics/testing/tools/#tagging-tests)

