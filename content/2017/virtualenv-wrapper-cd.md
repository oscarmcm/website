Title: VirtualenvWrapper auto cd
Date: 2017-08-10
Tags: en

**Virtualenv** is a tool to create isolated Python environments
for manage dependencies and versions, and indirectly permissions in your Python apps.
In comparison to _Virtualenv_, **Virtualenvwrapper** is a set of extensions
for creating and deleting virtual environments and otherwise managing your development workflow
Sometimes, manage your environments with _Virtualenvwrapper_ could be kinda tricky;
let's image this workflow.

```
~ > cd my_python_app
my_python_app > mkvirtualenv my_app
(my_app) my_python_app > |
```

Now anytime we want to work in our app we had to run something like this:

```
~ > cd my_python_app
my_python_app > workon my_app
(my_app) my_python_app > |
```

In other word, access to the app folder and run _workon env_name_ in order to active your environment;
mostly Python's developers feel this kinda awkard and thats why _Virtualenvwrapper_ offer
the <stron>- a</stron> option which can be used to associate an existing project
directory with the new environment.

```
~ > cd new_python_app
new_python_app > mkvirtualenv new_app -a $(pwd)
(new_app) new_python_app > |
```

Now, we can just write _workon env_name_ and this will auto access into your project folder

```
~ > workon new_app
(my_app) my_python_app > |
```

![VirtualEnv Wrapper]({attach}/images/virtualenv-wrapper-cd.png)

- [_VirtualEnv_](https://virtualenv.pypa.io/en/stable/)
- [_VirtualEnvWrapper_](https://virtualenvwrapper.readthedocs.io/en/latest/)
