Title: Git Dir
Date: 2017-07-13
Tags: en

With git 2.13 you get a feature called **conditional includes**
This means that I can have git automatically apply the correct name and
email address to my commits and I can keep my current source directory
layout on my machine. Let's imagine that you have a folder structure like this

```
/Users/phi/Code
  | --- CompanyX
  |      | ---- ProjectA
  |      | ---- ProjectB
  |      | ---- ProjectC
  | --- CompanyZ
  |      | ---- ProjectD
  | --- OSS
  |      | ---- AwesomeOSS
```

So, now trying to get a diffent git config (user/email) for each project would be
really hard and tired; here's when **Git Dir** comes to action:

1) Create a **.gitconfig** file in your Company folder (CompanyX for example)

```
~/Code/CompanyX/.gitconfig
```

2) Add your custom unique configs (user/email) for example

```
[user]
    name = Oscar
    email = oscar@the-companyx.com
```

3) Then add this into your main **.gitconfig** file

```
[includeIf "gitdir:~/Code/CompanyX"]
    path = ~/Code/CompanyX/.gitconfig
```

This approach could be replicated to any number of patterns you choose.
If you have specific accounts you use for client work you just add
more entries to the conditional block.

