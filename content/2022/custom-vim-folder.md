Title: Custom .vimrc configuration folder
Date: 2022-01-28
Tags: en

To achieve this we need special attention to the `autoload` folder and to the various plugins folders.

I wanted to move all vim configurations to the more (now) standard directory `.config` that is supposed to be used by all applications (even than some of them do not use it) and not pollute the *HOME* folder.

To be able to move all stuff vim needs to `.config/vim`, you need to make the following changes:

- on your shell rc file (`~/.bashrc` or `~/.zshrc`), add: `export VIMINIT="source ~/.config/vim/vimrc"` - This is to instruct vim where to find your new vimrc configuration file.
- on your `~/.config/vim/vimrc` file, before your plugin manager, set the autoload directory adding: `set runtimepath+=~/.config/vim,~/.config/vim/after` - This will instruct vim to run files and scripts found in those paths.

The current `runtimepath` can be seen with the command `set runtimepath?` and it will probably show something like `~/.vim,/var/lib/vim/addons,/usr/share/vim/vimfiles,/usr/share/vim/vim80,/usr/share/vim/vimfiles/after,/var/lib/vim/addons/after,~/.vim/after`.

You need to add the new folders in that list, without compromising the old folders, as they may have lots of default scripts.

Also you don't need `~/.vim` and `~/.vim/after`, as they don't exist, but no harm letting the path as is, and just add what you need.

- on your `vimrc` file, add: `set viminfo+=n~/.config/vim/viminfo` - This will tell vim to write the old `~/.viminfo` file in the new location, now without the prefixed dot.

### Plugins

If you don't have one installed, use the following lines in your `~/.config/vim/vimrc` file to download and auto-install it:

```
if empty(glob('~/.config/vim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
```

This will download and install your plugin manager. After that, the following lines will download and install a plugin of your choice (examplified with drbeco/vimtemplates and ajh16/VimCompleteMe plugins. You can add more as you wish.):

```
call plug#begin('~/.config/vim/plugged')
Plug 'drbeco/vimtemplates'
Plug 'ajh17/VimCompletesMe'
call plug#end()
```

This call will create folders like `~/.config/vim/plugged/vimtemplates` and `~/.config/vim/plugged/VimCompletesMe`, and so on, for each plugin.

So now, even the plugins are all organized inside `~/.config/vim`, and that really cleans the *HOME* directory.

And that's it.

