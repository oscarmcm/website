Title: Predicción de resultados de fútbol | Parte 2
Date: 2018-11-07
Tags: es
Slug: resultados-futbol-p2

En el [post anterior](/essays/2018/resultados-futbol-p1/) hablamos
sobre como determinar los goles marcados por _"X"_ equipo en un partido
de futbol, en este nuevo post veremos como automatizar este proceso.

Primero necesitamos encontrar una fuente de datos, que contenga los resultados
a utilizar para calcular los marcadores; el sitio de deportes **Marca** contiene
una [lista de resultados](https://www.marca.com/futbol/primera/clasificacion.html) que nos pueden ser utiles.

Dado que nuestro modelo se verá afectado por la condición del equipo, es decir,
si este juega de local o de visitante, es por ello que necesitamos
dividir nuestros datos según la condición del equipo.

- Resultados Global
- Resultados Local
- Resultados Visitante

Para cada una de las cabeceras, almacenaremos la siguiente información:

- Partidos Jugados -> **PJ**
- Partidos Ganados -> **PG**
- Partidos Empatados -> **PE**
- Partidos Perdidos -> **PP**
- Goles a Favor -> **GF**
- Goles en Contra -> **GC**
- Puntos -> **PT**

Tu tabla debe de verse de la siguiente manera:

![Excel Datos]({attach}/images/tabla-excel_datos.png)

Una vez que tengas la información necesaria, vamos a por
la segunda parte de la tabla, la cual debería de tener lo siguiente:

![Excel Calculos]({attach}/images/tabla-excel_calculos.png)

Ahora vamos a a generar cada uno de los cálculos:

- **Hf** -> GF/PJ (Local)
- **Hc** -> GC/PJ (Local)
- **Af** -> GF/PJ (Visitante)
- **Ac** -> GC/PJ (Visitante)

Seguido de esto, calcularemos los promedios, en esta parte usaremos los
resultados de los equipos, es por eso que tenemos que utilizar un rango
de datos y la función promedio de excel.

Ahora procedemos a calcular la media de goles marcados por X equipo
respecto a la media de TODOS los equipos segun su condición.

- **Hf_r** -> Hf/THf (Local)
- **Hc_r** -> Hc/THc (Local)
- **Af_r** -> Af/TAf (Visitante)
- **Af_r** -> Ac/TAc (Visitante)

En una nueva hoja podemos crear una tabla de distribución.
Con ella calcularemos los goles esperados del equipo
y la probabilidad de que se produzcan diferentes marcadores.

Para generar los goles esperados para un equipo de local usamos:
Hf_r * Ac_r * Thf -> Ataque equipo local * Defensa equipo visitante * Media total de
goles locales de la liga.

En el caso del equipo visitante usamos los mismos datos/cálculos solo que de forma inversa;
Ataque equipo visitante * Defensa equipo local * Media total de goles visitante de la liga.
Una vez que se tiene el número de goles esperados hay que obtener las probabilidades de que se produzca cada marcador.
En esta parte puedes dirigirte a mi [post anterior](/essays/2018/resultados-futbol-p1/), donde
explico como calcularlos.

![Excel Poisson]({attach}/images/tabla-excel_poisson.png)

Gracias por leer esta entrada, y si fue de tu agrado, no olvides compartir.
Puedes leer la siguiente parte [aqui](/essays/2018/resultados-futbol-p3/).

**PD:** Puedes descargar el excel haciendo [click aqui]({attach}/images/data/p.xlsx) :)

