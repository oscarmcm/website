Title: Django Places V3
Date: 2018-10-29
Tags: en

This is the most important release of Django Places, the project was
inactive for almost one year and surprisingly the GeoComplete lib was
archived and not maintained anymore in favor of the **"new"** _React GeoSuggest_.

This was a huge problem because the app started to have some bugs with maps
and the render process, so I decided to completely remove _GeoComplete_ and
_jQuery_ from Django Places.

After a successfully refactor for the JavaScript part with a custom/hand-made
simple lib, I decided to add support for inline admins and improve the way the project load
the required settings for the map and the marker.

If you want to learn more about it, just check the project in Github
at [oscarmcm/django-places](https://github.com/oscarmcm/django-places)
