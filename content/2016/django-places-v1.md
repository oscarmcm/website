Title: Django-Places v1
Date: 2016-04-28
Tags: en

Releasing the first stable version of Django Places, **CodeName: _Gingery Apple Sundaes_**.
It's a simple app for store places with **autocomplete** support and a related map.

### Why?

The main purpose of this app was, bring to the admin a simple
way to store a Point with: _Name_, _Latitude_, and _Longitude_.
The app uses the amazing [GeoComplete](https://github.com/ubilabs/geocomplete) lib by ubilabs.
For install just run this in you project:

```
pip install dj-places
```

### API

When you install **Django-Places** you have access to the following API:

```
>>> from myapp.models import ModelName
>>> poi = ModelName.objects.get(id=1)
>>> poi.position
Place('Metrocentro, Managua, Nicaragua', 52.522906, 13.41156)
>>> poi.position.place
'Metrocentro, Managua, Nicaragua'
>>> poi.position.latitude
52.522906
>>> poi.position.longitude
13.41156
```

If you want to learn more about it, just check the project in Github
at [oscarmcm/django-places](https://github.com/oscarmcm/django-places)

