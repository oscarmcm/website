Title: Netlandish Year One
Date: 2019-05-20
Tags: en

Has been 365 days since I started working for Netlandish Inc. I can’t imagine how
much in time it is, well, actually I know, but let’s talk about my journey and
all the things that I’ve learned so far, because its the fun part, trust me.

So, I joined Netlandish on May 21 of 2018, but the “negotiations” started like
one month before,  in that moment I was working on a small startup,
building their product using Ruby On Rails and React (the R3 stack), it also
included boxing classes two days per week (pretty awesome); now, let me put
some crazy scenarios happening in that moment:

- My country, Nicaragua, was in the middle of a political crisis (you can read more in the internet).
- Everything around me started to being really crazy, some friends leaving the country, others losing their jobs, small business in bankruptcy and, you know, all the crazy stuff.
- I was in the process of dev-testing for another company.

Crazy, right? So, on March 24 of 2018, my best friend Darwing asked me if I
was open for a new job, I said - well, let’s see what happens - I wasn’t fully
sure of do it, but then,  he mentioned a very important piece: “it’s a Python job,
and the guy is awesome, you’ll love this job”, so I replied him - let’s fu$^%& do it -
A few hours later, Sanchez, emailed me, asking if I was open to a new change,
here’s a small extract of the email:

> … Darwing said you’re stuck in Ruby land (so sorry to hear that haha!) and may be interested in switching back to Python based work…

[Mr. Sanchez](https://www.petersanchez.com/), is a self-taught Python developer with around _15_ years immersed in the language,
he prefer the Vim editor, less pager, and BSD based licenses, described by himself as a strange cat. Known for the ability to
bounce, rock, skate, and roll. (I don't know what does that mean); via Netlandish he worked with clients,
either directly or through larger agencies, like National Association of Realtors,
Chicago Transit Authority, National Geographic Channel, Washington University, HBO,
Sony, Fox SearchLight, HUGE, Beach Body, The Scripps Research Institute, and many other
smaller but just as important companies and organizations... ok, ok, ok, let’s go back to the main story.

As I said before, the things in the country started to being more crazy every
single day, and the job was remote, you know, I wasn’t fully sure;
I mean, do you want to start a new remote job in the middle of a political crisis?
I was scared homie! What happen if I don’t have the required skills, or if remote
working isn’t for me, what if the government create a new weird law, what
is home-office, is my internet provider good, trust me, this type of questions
(probably more) passed though my mind...

... but, well, finally a few days later we started talking about the position and
my role. It was fun, because the same day I talked with Sanchez,
was my final call with the other company, yeah, I failed, honestly it was
super complicated, to switch again to Python/Django after 1.6 years on the RoR land,
at least it helped me to remember more about the Python/Django world,
and thanks to _insert whatever you believe in_ everything just moved smoothly in
the call, yup, the test was super complicated too (trust me!), and then on May, 02 I accepted the
position to become a Netlandish employee... And on May, 21 I signed the contract,
because oh LOL, I forgot to print and sign it on time.

Well, the rest of it, is just a _Disney_ story... my first project was migrate an old,
very, very old Django app to a modern version, what a way to start it! but
we handle it like the champs we are:

[https://twitter.com/netlandish/status/1113872997339811840](https://twitter.com/netlandish/status/1113872997339811840).

Thanks to Netlandish I attended my first DjangoCon, and my first PyCon in the
US; also they give me the chance to speak in PyCaribbean and attend the PyColombia too, pretty f$%^ **AWESOME**,
and every day is just like the first day, learning a bunch of tips and tricks from an invaluable human in a
extraordinary distributed team.

I don’t have more words to describe this journey, just wanted to say thanks to
_Don Sanchez_ for all the good stories, for all the advices about the life's sense, and being the lighthouse on my
journey to become a better Python developer, to our distributed team for
all the good memes in slack and to _Darwing_, because without him this would never happened.

**UPDATE 22/05/19**

And lastly but no the last, thanks to my good friend [@RipeR81](https://twitter.com/RipeR81), thanks
for all the answers about remote-working, how those things work and the advices on how to setup
a good home-office, also, sorry for bothering you with that (see parragraph number 5),
you were the calm in the middle of the storm, thanks man.

Im excited to see what the world has prepared for me in the next months,
I know some details, and probably I'll blog about it, but they are super secret,
at least for now.

See ya’ll.

![NL Badge]({attach}/images/conf.jpg)
