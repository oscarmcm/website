Title: Distributing PyPI Packages using API Tokens in TravisCI
Date: 2019-08-11
Tags: es

**NOTE:** If you want to read the english version of this post, you can find it on my [Dev.To profile](https://dev.to/oscarmcm/distributing-pypi-packages-using-api-tokens-in-travisci-1n9i).

El codigo de ejemplo está disponible [en GitHub](https://github.com/oscarmcm/pypi-token-demo).

_PyPI_ es una de las herramientas que hace que **Python** sea tan poderoso, con solo un simple comando, obtienes acceso a miles de geniales bibliotecas, listas para usar en tu proyecto.

Escribir paquetes **Python** y desplegarlos en _PyPI_ es una forma de compartir bibliotecas con la comunidad de código abierto. Para los principiantes, enviar un paquete a _PyPI_ no es una tarea fácil, pero la comunidad está trabajando arduamente, para que este proceso sea más fácil, puede leer más sobre el empaquetado, siguiendo la [guía oficial de empaquetado en Python](https://packaging.python.org/tutorials/packaging-projects/).

En esta publicación, omitiré algunas cosas/pasos sobre el empaquetado y lo guiaré a través del proceso, para usar los nuevos [Tokens API](http://pyfound.blogspot.com/2019/07/pypi-now-supports-uploading-via-api.html) en _PyPI_, para usarse en tu servicio de CI/CD.

Para ello usaremos la instancia de [pruebas de PyPI](https://test.pypi.org/), que es una mejor opción para hacer la prueba inicial en tus paquetes, como probar si los documentos se procesan correctamente, los enlaces del proyecto o si los [clasificadores](https://pypi.org/classifiers/) en su _setup.py_ son correctos, por ejemplo.

## Usando Travis CI

_Travis CI_ puede liberar automáticamente su paquete **Python** a _PyPI_ después de una compilación exitosa. Para una configuración mínima, puedes usar el siguiente código en su archivo `.travis.yml`:

```yaml
deploy:
  provider: pypi
  user: "Your username"
  password: "Your password"
```

Sin embargo, como puedes observar, esto expondría tu contraseña de _PyPI_ al mundo, y esa es una muy mala idea. Lo que _Travis_ recomienda es encriptar la contraseña o guardar las credenciales en las variables de entorno de _Travis_, y agregarla a su _.travis.yml_, puede leer más al respecto en la [documentación de Travis](https://docs.travis-ci.com/user/deployment/pypi/).

### Obtener PyPI Tokens

Entonces, lo primero que debe hacer es ir a https://test.pypi.org/ y crear una cuenta (si no tiene una) o iniciar sesión, después de eso, necesitaremos nuestro _API Token_. ¿Cómo funciona? Vaya a la configuración de su cuenta _PyPI_ y seleccione "Add API token". Cuando crea un nuevo token, puedes elegir su alcance: puedes crear un token que se puede usar en todos los proyectos que mantiene o posee, o puede limitar su alcance a un solo proyecto.

![Add Token]({attach}/images/pypi-tokens/add-token.png)

### Como usarlo

Los tokens proporcionan una forma alternativa (en lugar de nombre de usuario y contraseña) para autenticarse al cargar paquetes en _PyPI_.

Estos tokens solo se pueden usar para cargar paquetes en _PyPI_, y no para iniciar sesión de manera más general, ya que un ladrón que copia el token tampoco tendrá la capacidad de eliminar el proyecto, eliminar versiones antiguas o, agregar o eliminar colaboradores.

Implementé con éxito la primera versión del proyecto de prueba en _PyPI_ manualmente, como puedes ver en la siguiente captura de pantalla:

![PyPI Token]({attach}/images/pypi-tokens/pypi-first.png)

Ahora necesitamos habilitar _Travis_ para este proyecto, así que vaya a **https://travis-ci.org/account/repositories** y habilítelo, después de eso tenemos que cambiar, las siguientes cosas en nuestro archivo `.travis.yml`:

- Cambiar el username a `__token__`.
- Cambiar la password con el valor del token, incluyendo el prefijo `pypi-`.

Para mayor seguridad, vamos a almacenar el token como una variable de entorno, vaya a `https://travis-ci.org/<owner>/<repository>/settings` y ahora nuestro archivo `.travis.yml` se verá así:

```yaml
deploy:
  provider: pypi
  user: __token__
  password: $TEST_PYPI_TOKEN
  server: https://test.pypi.org/legacy/
  distributions: "sdist bdist_wheel"
  on:
    branch: staging
    condition: $TRAVIS_PYTHON_VERSION = "3.6"
```

Y ahora, cada vez que insertamos código en la rama _staging_, _Travis_ activará un despliegue automático en la instancia de prueba _PyPI_.

Si tiene un caso de prueba simple en su aplicación, simplemente cambie el número de versión `setup.py` y envie el cambio a la rama _staging_, _Travis_ activará una compilación y, si todo funciona, desplegara la nueva versión en _PyPI_.

![TravisCI]({attach}/images/pypi-tokens/travisci.png)

Podemos inspeccionar los registros de compilación para ver si se implementó correctamente:

![TravisCI Log]({attach}/images/pypi-tokens/travisci-log.png)

Y voilà, en la linea _271_ vemos que el proceso de implementación comenzó, si expande la línea, podría ver lo siguiente:

![PyPI Deploy]({attach}/images/pypi-tokens/travisci-deploy.png)

Eso quiere decir que nuestra nueva versión, se subió correctamente y ahora la podemos ver disponible en _PyPI_, desplegada automáticamente con _Travis_ usando los nuevos API tokens de _PyPI_:


![PyPy Deployed]({attach}/images/pypi-tokens/pypi-second.png)

## Conclusión

Puedes leer más sobre los API tokens de _PyPI_ en el [blog oficial de la PSF](http://pyfound.blogspot.com/2019/07/pypi-now-supports-uploading-via-api.html) o ayudar a probar esta [caracteristica de prueba](https://wiki.python.org/psf/WarehousePackageMaintainerTesting).

Vamos, publica algo en _PyPI_.

