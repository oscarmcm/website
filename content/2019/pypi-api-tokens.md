Title: PyPI API Tokens
Date: 2019-08-06
Tags: es

Sí, el titulo no miente, PyPI ahora ofrece tokens para cargar nuestras librerías al registro, y si alguna vez intentaste automatizar este proceso usando algún servicio de *CI/CD* como [Travis](https://travis-ci.org/) es muy probable que no te sientas tan cómodo compartiendo tus contraseñas y que esta sea la mejor noticia.

En los últimos meses, se han agregado diferentes métodos de seguridad para el inicio de sesión como: autenticación de dos factores (2FA) y el soporte de contraseña única (TOTP) basada en tiempo a fines de mayo y soporte de dispositivo de seguridad física a mediados de junio. Más de 1600 usuarios han comenzado a usar dispositivos de seguridad física o aplicaciones TOTP para proteger mejor sus cuentas, durante la semana pasada, más del 7.8% de los inicios de sesión en PyPI.org han sido protegidos por 2FA, frente al 3% en el mes de junio (_Fuente_: http://pyfound.blogspot.com).

Pues bien, la mas reciente mejora hacia PyPI, es que ahora podemos usar [API tokens](https://pypi.org/help/#apitoken) para cargar nuestros paquetes, tanto al registro principal, como al de registro de [pruebas](https://packaging.python.org/guides/using-testpypi/).

**NOTA** esta nueva funcionalidad aun esta en fase de pruebas, que tú puedes [ayudar a probar](https://wiki.python.org/psf/WarehousePackageMaintainerTesting).

# Como funciona?

Primero tienes que ir a tu cuenta de PyPI, y en el apartado de _"Account settings"_, casi al final se encuentra una sección, para agregar un nuevo API token:

![PyPI Token]({attach}/images/pypi-token-home.png)

Y selecciona la opción _"Add API token"_, una vez seleccionado, en la siguiente pagina veras el formulario para crear un nuevo token:

![Token Form Step 1]({attach}/images/pypi-token-form.png)

Aquí puedes elegir el alcance de tu nuevo token: puedes crear un token que puede cargar a todos los proyectos que mantienes o posees, o puedes limitar su alcance a un solo proyecto.

![Token Form Step 2]({attach}/images/pypi-token-form2.png)

Una vez creado el token, recuerda guardarlo, por que sera la única vez que lo puedas ver; en la pantalla de administración de tokens puedes ver cuándo se crearon y utilizaron por última vez cada uno, puedes revocar un token sin revocar otros, y sin tener que cambiar tu contraseña en PyPI o en los archivos de configuración.

![Token List]({attach}/images/pypi-token-list.png)

Y listo, ahora puedes usar los tokens para cargar de forma automatizada tus paquetes al registro; en el proximo post, explicare como usar los tokens con tu servicio de CI/CD.

Puedes leer el post original en el [blog de la Python Software Foundation.](http://pyfound.blogspot.com/2019/07/pypi-now-supports-uploading-via-api.html)

